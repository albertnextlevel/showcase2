export * from '@app/nli-components/nli-timeline/nli-timeline.component';
export * from '@app/nli-components/nli-text/nli-text.component';
export * from '@app/nli-components/nli-select/nli-select.component';
export * from '@app/nli-components/nli-password/nli-password.component';
export * from '@app/nli-components/nli-longtext/nli-longtext.component';
export * from '@app/nli-components/nli-fileupload/nli-fileupload.component';
export * from '@app/nli-components/nli-menu/nli-menu.component';
export * from '@app/nli-components/nli-searchbar/nli-searchbar.component';

export const components = [
    {
        name: 'NliTimelineComponent',
        title: 'Timeline',
        path: '@app/nli-components/nli-timeline/nli-timeline.component',
        url: 'timeline'
    },
    {
        name: 'NliTextComponent',
        title: 'Text',
        path: '@app/nli-components/nli-text/nli-text.component',
        url: 'text'
    },
    {
        name: 'NliPasswordComponent',
        title: 'Password',
        path: '@app/nli-components/nli-password/nli-password.component',
        url: 'password'
    },
    {
        name: 'NliSelectComponent',
        title: 'Select',
        path: '@app/nli-components/nli-select/nli-select.component',
        url: 'select'
    },
    {
        name: 'NliLongTextComponent',
        title: 'Long Text',
        path: '@app/nli-components/nli-longtext/nli-longtext.component',
        url: 'longtext'
    },
    {
        name: 'NliFileUploadComponent',
        title: 'File Upload',
        path: '@app/nli-components/nli-fileupload/nli-fileupload.component',
        url: 'fileupload'
    },
    {
        name: 'NliMenuComponent',
        title: 'Menu',
        path: '@app/nli-components/nli-menu/nli-menu.component',
        url: 'menu'
    },
    {
        name: 'NliSearchBarComponent',
        title: 'Search Bar',
        path: '@app/nli-components/nli-searchbar/nli-searchbar.component',
        url: 'searchbar'
    },
    ];