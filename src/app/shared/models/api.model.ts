interface ApiPropertyModel {
  name: string;
  description: string;
}

interface ApiMethodModel {
  name: string;
  description: string;
  returns?: string;
}

interface ApiItemModel {
  name: string;
  description: string;
  properties?: ApiPropertyModel[];
  methods?: ApiMethodModel[];
}

export interface ApiModel {
  type: string;
  items: ApiItemModel[];
}
