export interface SetupModel {
  description: string;
  code: string;
  language: string;
}
