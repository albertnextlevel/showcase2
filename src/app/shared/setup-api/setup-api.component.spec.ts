import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupApiComponent } from './setup-api.component';

describe('SetupApiComponent', () => {
  let component: SetupApiComponent;
  let fixture: ComponentFixture<SetupApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
