import { Component, Input } from '@angular/core';
import { SetupModel } from '../models/setup.model';

@Component({
  selector: 'nli-setup-api',
  templateUrl: './setup-api.component.html',
  styleUrls: ['./setup-api.component.scss']
})
export class SetupApiComponent {

  @Input() setup: SetupModel[];
  @Input() api: [];
  constructor() { }

}
