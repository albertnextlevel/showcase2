import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightModule } from 'ngx-highlightjs';

import { MaterialModule } from '@app/material.module';

import { LoaderComponent } from './loader/loader.component';

import { UsageComponent } from './usage/usage.component';
import { SetupApiComponent } from './setup-api/setup-api.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HighlightModule,
    TranslateModule
  ],
  declarations: [
    LoaderComponent,
    UsageComponent,
    SetupApiComponent
  ],
  exports: [
    LoaderComponent,
    UsageComponent,
    SetupApiComponent
  ]
})
export class SharedModule { }
