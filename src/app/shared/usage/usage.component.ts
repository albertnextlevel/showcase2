import { Component, Input } from '@angular/core';

@Component({
  selector: 'nli-usage',
  templateUrl: './usage.component.html',
  styleUrls: ['./usage.component.scss']
})
export class UsageComponent {

  @Input() usage: any;
  constructor() { }

}
