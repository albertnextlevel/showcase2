import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { NliComponentsRoutingModule } from './nli-components-routing.module';
import { NliComponentsComponent } from './nli-components.component';

import * as components from '@app/config/components.config';

import { LibModule as TimelineModule } from '@next-level-integration/nli-timeline-lib';
import { LibModule  as  NliInputLib } from '@next-level-integration/nli-input-lib';
import { LibModule as NliMenuModule } from '@next-level-integration/nli-menu-lib';
import { LibModule as NliSearchBarComponent } from '@next-level-integration/nli-search-bar-lib';

import { SharedModule } from '@app/shared';

const module = {
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    SharedModule,
    NliComponentsRoutingModule,
    TimelineModule,
    NliInputLib,
    NliMenuModule,
    NliSearchBarComponent
  ],
  declarations: [
    NliComponentsComponent,
  ]
};

components.components.forEach(component => {
  module.declarations.push(components[component.name]);
});

@NgModule(module)
export class NliComponentsModule { }
