import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';


@Component({
  selector: 'nli-nli-menu',
  templateUrl: './nli-menu.component.html',
  styleUrls: ['./nli-menu.component.scss']
})
export class NliMenuComponent implements OnInit {


  usage: any = {
    ts: ``,
    html: `<nli-menu  [firstLineText]="'John Doe'" [secoundLineText]="'Sample User'">
    <nli-menu-item [text]="'Hello'" (click)="say('world')"></nli-menu-item>
    <nli-menu-item [icon]="'logout'" [text]="'Logout'" (click)="say('You want to logout?')" iconPath="../assets/home/logout.svg"></nli-menu-item>
</nli-menu>`,
    css: ``
  };

  setup: SetupModel[] = [
    {
      description: `First part would be to install the npm package:`,
      code: `npm install --save @next-level-integration/nli-menu-lib`,
      language: 'bash'
    },
    {
      description: `To make icons work correctly we must add the following to our index html:`,
      code: `<head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic"
      type="text/css" rel="stylesheet">
   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>`,
      language: 'xml'
    },
    {
      description: `Import the module to our app:`,
      code: `import { LibModule as NliMenuModule} from '@next-level-integration/nli-menu-lib';';
      @NgModule({
        declarations: [],
        imports: [
          NliMenuModule
        ],
        providers: [],
        bootstrap: [AppComponent]
});`,
      language: 'typescript'
    },
    {
      description: `Apply our theme customization to the component:`,
      code: `@import '~@next-level-integration/nli-menu-lib/lib/menu/item/item.component.scss';
      @include nli-material-theme($app-theme);`,
      language: 'scss'
    },
    {
      description: `And also modify the main tsconfig file to include the component\`s ts files:`,
      code: `"include": [
        "node_modules/@next-level-integration/**/*.ts",
        "src/**/*.ts"
]`,
      language: 'json'
    }
  ];

  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-menu',
          description: `Menu directive`,
          properties: [
            {
              name: `@Input()
              id:string ='nliMenuComponent'`,
              description: ``
            },
            {
              name: `@Input()
              firstLineText:string`,
              description: ``
            },
            {
              name: `@Input()
              secoundLineText:string`,
              description: ``
            },
            {
              name: `@Input()
              placeHolder:string`,
              description: ``
            },
            {
              name: `@Input()
              public isToolBarMenu:boolean`,
              description: ``
            },
          ],
          methods: []
        },
        {
          name: 'nli-menu-item',
          description: `Item inside the menu`,
          properties: [
            {
              name: `@Input()
              id:string='nliMenuItemComponent'`,
              description: ``
            },
            {
              name: `@Input()
              icon:string`,
              description: ``
            },
            {
              name: `@Input()
              text:string`,
              description: ``
            },
            {
              name: `@Input()
              iconPath:string`,
              description: ``
            },
            {
              name: `@Input()
              disabled:boolean = false`,
              description: ``
            },
          ],
          methods: [{
            name: `click`,
            description: `Handles item click`,
            returns: ``
          }]
        }
      ]
    },
  ];


  constructor() {}

  ngOnInit() {}

  say(message) {
    alert(message);
  }

}
