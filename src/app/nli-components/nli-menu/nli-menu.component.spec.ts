import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliMenuComponent } from './nli-menu.component';

describe('NliMenuComponent', () => {
  let component: NliMenuComponent;
  let fixture: ComponentFixture<NliMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
