import { Component, OnInit } from '@angular/core';

import { components } from '@app/config/components.config';


@Component({
  selector: 'nli-nli-components',
  templateUrl: './nli-components.component.html',
  styleUrls: ['./nli-components.component.scss']
})
export class NliComponentsComponent implements OnInit {

  componentsList = components;

  constructor() { }

  ngOnInit() {
  }

}
