import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliSelectComponent } from './nli-select.component';

describe('NliSelectComponent', () => {
  let component: NliSelectComponent;
  let fixture: ComponentFixture<NliSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
