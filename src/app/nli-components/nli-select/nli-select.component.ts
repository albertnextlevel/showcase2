import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'nli-nli-select',
  templateUrl: './nli-select.component.html',
  styleUrls: ['./nli-select.component.scss']
})
export class NliSelectComponent implements OnInit {

  public testForm!: FormGroup;

  
  citiesList = [{
    label: 'Cologne',
    value: 'cologne'
  },
  {
    label: 'Brasov',
    value: 'brasov'
  }];

  usage: any = {
    ts: `public citiesList = [{
      label: 'Cologne',
      value: 'cologne'
    },
    {
      label: 'Brasov',
      value: 'brasov'
}];
public loginForm!: FormGroup;

constructor(private fb: FormBuilder) {
      this.testForm = fb.group({
        cities: ['', Validators.required]
});`,
    html: `<nli-select name="selectCity"
    staticText="Please select a city"
    inputLabel="City"  [dataList]="citiesList"
    inputClass="white-combo" [formControl]="testForm.controls['cities']">
</nli-select>`,
    css: ``
  };

  setup: SetupModel[] = [
    {
      description: `Install npm package:`,
      code: `npm install --save @next-level-integration/nli-input-lib`,
      language: 'bash'
    },
    {
      description: `To make icons work correctly we must add the following to our index html:`,
      code: `<head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic"
            type="text/css" rel="stylesheet">
</head>`,
      language: 'xml'
    },
    {
      description: `Import the module to our app:`,
      code: `import {LibModule  as  NliInputLib} from  '@next-level-integration/nli-input-lib';';
      @NgModule({
        declarations: [],
        imports: [
          NliInputLib
        ],
        providers: [],
        bootstrap: [AppComponent]
});`,
      language: 'typescript'
    },
    {
      description: `Apply our theme customization to the component:`,
      code: `@import '~@next-level-integration/nli-input-lib/lib/nli-select/nli-select.component.scss';
@include nli-material-theme($nli-theme);`,
      language: 'scss'
    },
    {
      description: `And also modify the main tsconfig file to include the component\`s ts files:`,
      code: `"include": [
        "node_modules/@next-level-integration/**/*.ts",
        "src/**/*.ts"
]`,
      language: 'json'
    }
  ];

  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-select',
          description: `Select dropdown input directive`,
          properties: [
            {
              name: `@Input()
              id: string='nli-select'`,
              description: ``
            },
            {
              name: `@Input()
              staticText: string = ''`,
              description: ``
            },
            {
              name: `@Input()
              defaultLable: string = ''`,
              description: ``
            },
            {
              name: `@Input()
              defaultValue: any = ''`,
              description: ``
            },
            {
              name: `@Input()
              hasDefault: boolean = false`,
              description: ``
            },
            {
              name: `@Input()
              dataList: any[]`,
              description: ``
            },
            {
              name: `@Input()
              inputLabel: string`,
              description: ``
            },
            {
              name: `@Input()
              errorMsg: string`,
              description: ``
            },
            {
              name: `@Input()
              formControl = new FormControl(null, null)`,
              description: ``
            },
            {
              name: `@Input()
              inputClass: string`,
              description: ``
            },
            {
              name: `@Input()
              isNativeInput: boolean`,
              description: ``
            },
            {
              name: `@Input()
              returnClass: boolean`,
              description: ``
            },
          ],
          methods: []
        },
      ]
    }
  ];


  constructor(private fb: FormBuilder) {
    this.testForm = fb.group({
      cities: ['', Validators.required]
    });
  }

  ngOnInit() {}
}
