import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliLongTextComponent } from './nli-longtext.component';

describe('NliLongTextComponent', () => {
  let component: NliLongTextComponent;
  let fixture: ComponentFixture<NliLongTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliLongTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliLongTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
