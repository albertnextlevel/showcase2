import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliTimelineComponent } from './nli-timeline.component';

describe('NliTimelineComponent', () => {
  let component: NliTimelineComponent;
  let fixture: ComponentFixture<NliTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
