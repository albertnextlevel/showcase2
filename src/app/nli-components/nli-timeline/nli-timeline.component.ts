import { Component, OnInit } from '@angular/core';

import { TimeLineStepModel } from '@next-level-integration/nli-timeline-lib/lib/timeline/timelineitem/timelineitem-model';
import { TimeLineModel } from '@next-level-integration/nli-timeline-lib/lib/timeline/timeline-model';
import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';

@Component({
  selector: 'nli-nli-timeline',
  templateUrl: './nli-timeline.component.html',
  styleUrls: ['./nli-timeline.component.scss']
})
export class NliTimelineComponent implements OnInit {
  timelineModel: TimeLineModel = new TimeLineModel(5, [
    new TimeLineStepModel('17', 'Zählerwechsel17', new Date('2014-04-03')),
    new TimeLineStepModel('18', 'Zählerwechsel18', new Date('2015-04-03')),
    new TimeLineStepModel('1', 'Zählerwechsel1', new Date('2016-04-03')),
    new TimeLineStepModel('2', 'Zählerwechsel2', new Date('2017-04-03')),
    new TimeLineStepModel('3', 'Zählerwechsel3', new Date('2018-04-03')),
    new TimeLineStepModel('4', 'Zählerwechsel4', new Date('2019-04-03')),
    new TimeLineStepModel('5', 'Zählerwechsel5', new Date('2020-04-03')),
    new TimeLineStepModel('6', 'Zählerwechsel6', new Date('2013-04-03')),
    new TimeLineStepModel('7', 'Zählerwechsel7', new Date('2012-04-03')),
    new TimeLineStepModel('8', 'Zählerwechsel8', new Date('2011-04-03')),
    new TimeLineStepModel('9', 'Zählerwechsel9', new Date('2010-04-03')),
    new TimeLineStepModel('10', 'Zählerwechsel10', new Date('2009-04-03')),
    new TimeLineStepModel('11', 'Zählerwechsel11', new Date('2008-04-03')),
    new TimeLineStepModel('12', 'Zählerwechsel12', new Date('2007-04-03')),
    new TimeLineStepModel('13', 'Zählerwechsel13', new Date('2006-04-03')),
    new TimeLineStepModel('14', 'Zählerwechsel14', new Date('2003-04-03')),
    new TimeLineStepModel('15', 'Zählerwechsel15', new Date('2002-04-03')),
    new TimeLineStepModel('16', 'Zählerwechsel16', new Date('2000-11-11'))
  ]);

  usage: any = {
    ts: `import { TimeLineStepModel } from '@next-level-integration/nli-timeline-lib/lib/timeline/timelineitem/timelineitem-model';
import { TimeLineModel } from '@next-level-integration/nli-timeline-lib/lib/timeline/timeline-model';

timelineModel: TimeLineModel = new TimeLineModel(5, [
  new TimeLineStepModel('17','Zählerwechsel17', new Date('2014-04-03')),
  new TimeLineStepModel('18','Zählerwechsel18', new Date('2015-04-03')),
  new TimeLineStepModel('1','Zählerwechsel1', new Date('2016-04-03')),
  new TimeLineStepModel('2','Zählerwechsel2', new Date('2017-04-03')),
  new TimeLineStepModel('3','Zählerwechsel3', new Date('2018-04-03')),
  new TimeLineStepModel('4','Zählerwechsel4', new Date('2019-04-03')),
  new TimeLineStepModel('5','Zählerwechsel5', new Date('2020-04-03')),
  new TimeLineStepModel('6','Zählerwechsel6', new Date('2013-04-03')),
  new TimeLineStepModel('7','Zählerwechsel7', new Date('2012-04-03')),
  new TimeLineStepModel('8','Zählerwechsel8', new Date('2011-04-03')),
  new TimeLineStepModel('9','Zählerwechsel9', new Date('2010-04-03')),
  new TimeLineStepModel('10','Zählerwechsel10', new Date('2009-04-03')),
  new TimeLineStepModel('11','Zählerwechsel11', new Date('2008-04-03')),
  new TimeLineStepModel('12','Zählerwechsel12', new Date('2007-04-03')),
  new TimeLineStepModel('13','Zählerwechsel13', new Date('2006-04-03')),
  new TimeLineStepModel('14','Zählerwechsel14', new Date('2003-04-03')),
  new TimeLineStepModel('15','Zählerwechsel15', new Date('2002-04-03')),
  new TimeLineStepModel('16','Zählerwechsel16', new Date('2000-11-11'))
]);`,

    html: `<div class="timeline-container">
      <nli-timeline [showDatePicker]="true" [visibleSteps]="14">
        <nli-timelineitem
            *ngFor="let timeLineStepModel of timelineModel.steps let i=index "
            [model]="timeLineStepModel">
        </nli-timelineitem>
      </nli-timeline>
</div> `,

    css: `.timeline-container {
  width: 100%;
}`
  };

  setup: SetupModel[] = [
    {
      description: `First part would be to install the npm package:`,
      code: `npm install --save @next-level-integration/nli-timeline-lib`,
      language: 'bash'
    },
    {
      description: `To make icons work correctly we must add the following to our index html:`,
      code: `<head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic"
            type="text/css" rel="stylesheet">
</head>`,
      language: 'xml'
    },
    {
      description: `Import the module to our app:`,
      code: `import { LibModule as TimelineModule} from '@next-level-integration/nli-timeline-lib';
      @NgModule({
        declarations: [],
        imports: [
          TimelineModule
        ],
        providers: [],
        bootstrap: [AppComponent]
});`,
      language: 'typescript'
    },
    {
      description: `Apply our theme customization to the component:`,
      code: `@import '~@next-level-integration/nli-timeline-lib/lib/timeline/timeline.component.scss';
@include nli-material-theme($nli-theme);`,
      language: 'scss'
    },
    {
      description: `And also modify the main tsconfig file to include the component\`s ts files:`,
      code: `"include": [
        "node_modules/@next-level-integration/**/*.ts",
        "src/**/*.ts"
]`,
      language: 'json'
    }
  ];

  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-timeline',
          description: `Main timeline directive`,
          properties: [
            {
              name: `@Input()
              showDatePicker: boolean`,
              description: `Show or hide the datepicker`
            },
            {
              name: `@Input()
              visibleSteps: number`,
              description: `Number of steps to display`
            }
          ],
          methods: [{
            name: `onSelect`,
            description: `Handles user selection`,
            returns: ``
          }]
        },
        {
          name: 'nli-timelineitem',
          description: `Item inside the timeline`,
          properties: [
            {
              name: `@Input()
              model: TimeLineModel`,
              description: `Timeline model`
            }
          ],
          methods: []
        }
      ]
    },

    {
      type: 'Classes',
      items: [
        {
          name: 'TimeLineModel',
          description: `Main timeline model`,
          properties: [
            {
              name: `visibleSteps: number`,
              description: `Visible steps`
            },
            {
              name: `currentPage: number`,
              description: `Current page`
            },
            {
              name: `steps: TimeLineStepModel[]`,
              description: `Timeline step`
            },
            {
              name: `lastVisibleStepCount: number`,
              description: `???`
            }
          ],
          methods: []
        },
        {
          name: 'TimeLineStepModel',
          description: `Step inside the timeline model`,
          properties: [
            {
              name: `id: string`,
              description: `Id of step`
            },
            {
              name: `date: Date`,
              description: `Date of step`
            },
            {
              name: `width: number`,
              description: `Step width`
            },
            {
              name: `selected: boolean`,
              description: `Selected status`
            },
            {
              name: `name: string`,
              description: `Step name`
            }
          ],
          methods: []
        }
      ]
    }
  ];


  constructor() {}

  ngOnInit() {}
}
