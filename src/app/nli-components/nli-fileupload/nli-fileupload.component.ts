import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EventEmitter } from 'events';


@Component({
  selector: 'nli-nli-fileupload',
  templateUrl: './nli-fileupload.component.html',
  styleUrls: ['./nli-fileupload.component.scss']
})
export class NliFileUploadComponent implements OnInit {

  public testForm!: FormGroup;


  usage: any = {
    ts: `public testForm!: FormGroup;
    
constructor(private fb: FormBuilder) {
      this.testForm = fb.group({
        cities: ['', Validators.required]
      });
}

uploaded(files: FileList) {
      alert(\`You have selected \$\{files[0].name\}\`);
}`,
    html: `<nli-file-upload name="uploadPhoto"
    inputLabel="Upload Photo"
    [formControl]="testForm.controls['photo']"
    (onFileUpload)="uploaded($event)">
</nli-file-upload>`,
    css: ``
  };

  setup: SetupModel[] = [
    {
      description: `Install npm package:`,
      code: `npm install --save @next-level-integration/nli-input-lib`,
      language: 'bash'
    },
    {
      description: `To make icons work correctly we must add the following to our index html:`,
      code: `<head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic"
            type="text/css" rel="stylesheet">
</head>`,
      language: 'xml'
    },
    {
      description: `Import the module to our app:`,
      code: `import {LibModule  as  NliInputLib} from  '@next-level-integration/nli-input-lib';';
      @NgModule({
        declarations: [],
        imports: [
          NliInputLib
        ],
        providers: [],
        bootstrap: [AppComponent]
});`,
      language: 'typescript'
    },
    {
      description: `Apply our theme customization to the component:`,
      code: `@import '~@next-level-integration/nli-input-lib/lib/nli-file-upload/nli-file-upload.component.scss';
@include nli-material-theme($nli-theme);`,
      language: 'scss'
    },
    {
      description: `And also modify the main tsconfig file to include the component\`s ts files:`,
      code: `"include": [
        "node_modules/@next-level-integration/**/*.ts",
        "src/**/*.ts"
]`,
      language: 'json'
    }
  ];

  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-file-upload',
          description: `File upload directive`,
          properties: [
            {
              name: `@Input()
              id: string='nli-file-upload'`,
              description: ``
            },
            {
              name: `@Input()
              inputLabel: string`,
              description: ``
            },
            {
              name: `@Input()
              readonly: boolean`,
              description: ``
            },
            {
              name: `@Input()
              disabled: boolean`,
              description: ``
            },
            {
              name: `@Input()
              icon: string`,
              description: ``
            },
            {
              name: `@Input()
              iconColor: string`,
              description: ``
            },
            {
              name: `@Input()
              accept: string`,
              description: ``
            },
            {
              name: `@Input()
              errorMsg: string`,
              description: ``
            },
            {
              name: `@Input()
              hintMsg: string`,
              description: ``
            },
            {
              name: `@Input()
              nliMaxLength: string`,
              description: ``
            },
            {
              name: `@Input()
              formControl`,
              description: ``
            },
          ],
          methods: [{
            name: `onFileUpload`,
            description: `EventEmitter<FileList>`,
            returns: `FileList`
          }]
        },
      ]
    }
  ];


  constructor(private fb: FormBuilder) {
    this.testForm = fb.group({
      cities: ['', Validators.required]
    });
  }

  ngOnInit() {}

  uploaded(files: FileList) {
    alert(`You have selected ${files[0].name}`);
  }
}
