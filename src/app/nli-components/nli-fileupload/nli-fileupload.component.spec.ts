import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliFileUploadComponent } from './nli-fileupload.component';

describe('NliFileUploadComponent', () => {
  let component: NliFileUploadComponent;
  let fixture: ComponentFixture<NliFileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliFileUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
