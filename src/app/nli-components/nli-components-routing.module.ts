import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { MultiComponent } from '@app/shell/multi/multi.component';
import { NliComponentsComponent } from './nli-components.component';

import * as components from '@app/config/components.config';

let componentRoutes = [
    { path: '', component: NliComponentsComponent, data: { title: extract('Components') } }
];

components.components.forEach(component => {
  componentRoutes.push({ path: component.url, component: components[component.name], data: { title: extract(component.title) } });
});

const routes: Routes = [
  Shell.childRoutes([
    { path: 'components', component: MultiComponent, data: { title: extract('Components') }, children: componentRoutes }
  ])
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class NliComponentsRoutingModule { }
