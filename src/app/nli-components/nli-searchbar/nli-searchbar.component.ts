import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';


@Component({
  selector: 'nli-nli-searchbar',
  templateUrl: './nli-searchbar.component.html',
  styleUrls: ['./nli-searchbar.component.scss']
})
export class NliSearchBarComponent implements OnInit {


  usage: any = {
    ts: ``,
    html: `<nli-search-bar placeHolder="Search" (onSearch)="search($event)" value="Search...">
</nli-search-bar>`,
    css: ``
  };

  setup: SetupModel[] = [
    {
      description: `First part would be to install the npm package:`,
      code: `npm install --save @next-level-integration/nli-timeline-lib`,
      language: 'bash'
    }
  ];


  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-search-bar',
          description: `Search bar directive`,
          properties: [
            {
              name: `@Input() id: string="searchBar"`,
              description: ``
            },
            {
              name: `@Input() value: string`,
              description: ``
            },
            {
              name: `@Input() placeHolder: string`,
              description: ``
            }
          ],
          methods: [{
            name: `onSearch`,
            description: `Handles search upon enter button press`,
            returns: ``
          }]
        },
      ]
    },
  ];


  constructor() {}

  ngOnInit() {}

  search(searched) {
    alert(searched);
  }
}
