import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliSearchBarComponent } from './nli-searchbar.component';

describe('NliSearchBarComponent', () => {
  let component: NliSearchBarComponent;
  let fixture: ComponentFixture<NliSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
