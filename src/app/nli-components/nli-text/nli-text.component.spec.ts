import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliTextComponent } from './nli-text.component';

describe('NliTextComponent', () => {
  let component: NliTextComponent;
  let fixture: ComponentFixture<NliTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
