import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'nli-nli-text',
  templateUrl: './nli-text.component.html',
  styleUrls: ['./nli-text.component.scss']
})
export class NliTextComponent implements OnInit {


  public loginForm!: FormGroup;
  
  usage: any = {
    ts: `public loginForm!: FormGroup;

constructor(private fb: FormBuilder) {
      this.loginForm = fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
});`,
    html: `<nli-text name="username"
    [formControl]="loginForm.controls['username']"
    inputLabel="Username"
    inputClass="sample-input"
    errorMsg="This field is mandatory.">
    </nli-text>`,
    css: `.sample-input {
      width: 100%;
}`
  };

  setup: SetupModel[] = [
    {
      description: `Install npm package:`,
      code: `npm install --save @next-level-integration/nli-input-lib`,
      language: 'bash'
    },
    {
      description: `To make icons work correctly we must add the following to our index html:`,
      code: `<head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic"
            type="text/css" rel="stylesheet">
</head>`,
      language: 'xml'
    },
    {
      description: `Import the module to our app:`,
      code: `import {LibModule  as  NliInputLib} from  '@next-level-integration/nli-input-lib';';
      @NgModule({
        declarations: [],
        imports: [
          NliInputLib
        ],
        providers: [],
        bootstrap: [AppComponent]
});`,
      language: 'typescript'
    },
    {
      description: `Apply our theme customization to the component:`,
      code: `@import '~@next-level-integration/nli-input-lib/lib/nli-text/nli-text.component.scss';
@include nli-material-theme($nli-theme);`,
      language: 'scss'
    },
    {
      description: `And also modify the main tsconfig file to include the component\`s ts files:`,
      code: `"include": [
        "node_modules/@next-level-integration/**/*.ts",
        "src/**/*.ts"
]`,
      language: 'json'
    }
  ];

  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-text',
          description: `Text input directive`,
          properties: [
            {
              name: `@Input()
              inputLabel: string`,
              description: ``
            },
            {
              name: `@Input()
              id: string='nli-text'`,
              description: ``
            },
            {
              name: `@Input()
              readonly: boolean`,
              description: ``
            },
            {
              name: `@Input()
              disabled: boolean`,
              description: ``
            },
            {
              name: `@Input()
              icon: string`,
              description: ``
            },
            {
              name: `@Input()
              iconColor: string`,
              description: ``
            },
            {
              name: `@Input()
              errorMsg: string`,
              description: ``
            },
            {
              name: `@Input()
              hintMsg: string`,
              description: ``
            },
            {
              name: `@Input()
              nliMaxLength: string`,
              description: ``
            },
            {
              name: `@Input()
              public mask: Array<string | RegExp> = null`,
              description: ``
            },
            {
              name: `@Input()
              formControl = new FormControl(null, null)`,
              description: ``
            },
            {
              name: `@Input()
              translator: Translator`,
              description: ``
            },
            {
              name: `@Input()
              inputClass:string`,
              description: ``
            },
          ],
          methods: [{
            name: `onClicked`,
            description: `EventEmitter`,
            returns: ``
          },
          {
            name: `onFocus`,
            description: `EventEmitter`,
            returns: ``
          }]
        },
      ]
    }
  ];


  constructor(private fb: FormBuilder) {
    this.loginForm = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {}
}
