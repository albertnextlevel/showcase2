import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliComponentsComponent } from './nli-components.component';

describe('NliComponentsComponent', () => {
  let component: NliComponentsComponent;
  let fixture: ComponentFixture<NliComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
