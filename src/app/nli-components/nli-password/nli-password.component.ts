import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'nli-nli-password',
  templateUrl: './nli-password.component.html',
  styleUrls: ['./nli-password.component.scss']
})
export class NliPasswordComponent implements OnInit {


  public loginForm!: FormGroup;
  
  usage: any = {
    ts: `public loginForm!: FormGroup;

constructor(private fb: FormBuilder) {
      this.loginForm = fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
});`,
    html: `<nli-password  name="password"
    [formControl]="loginForm.controls['password']"
    inputLabel="Password"
    inputClass="sample-input"
    errorMsg="This field is mandatory.">
    </nli-password>`,
    css: `.sample-input {
      width: 100%;
}`
  };

  setup: SetupModel[] = [
    {
      description: `Install npm package:`,
      code: `npm install --save @next-level-integration/nli-input-lib`,
      language: 'bash'
    },
    {
      description: `To make icons work correctly we must add the following to our index html:`,
      code: `<head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic"
            type="text/css" rel="stylesheet">
</head>`,
      language: 'xml'
    },
    {
      description: `Import the module to our app:`,
      code: `import {LibModule  as  NliInputLib} from  '@next-level-integration/nli-input-lib';';
      @NgModule({
        declarations: [],
        imports: [
          NliInputLib
        ],
        providers: [],
        bootstrap: [AppComponent]
});`,
      language: 'typescript'
    },
    {
      description: `Apply our theme customization to the component:`,
      code: `@import '~@next-level-integration/nli-input-lib/lib/nli-password/nli-password.component.scss';
@include nli-material-theme($nli-theme);`,
      language: 'scss'
    },
    {
      description: `And also modify the main tsconfig file to include the component\`s ts files:`,
      code: `"include": [
        "node_modules/@next-level-integration/**/*.ts",
        "src/**/*.ts"
]`,
      language: 'json'
    }
  ];


  api: ApiModel[] = [
    {
      type: 'Directives',
      items: [
        {
          name: 'nli-password',
          description: `Password input directive`,
          properties: [
            {
              name: `@Input()
              inputLabel: string`,
              description: ``
            },
            {
              name: `@Input()
              id: string='nli-password'`,
              description: ``
            },
            {
              name: `@Input()
              errorMsg: string`,
              description: ``
            },
            {
              name: `@Input()
              formControl`,
              description: ``
            },
            {
              name: `@Input()
              inputClass:string`,
              description: ``
            },
          ],
          methods: []
        },
      ]
    }
  ];


  constructor(private fb: FormBuilder) {
    this.loginForm = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {}
}
