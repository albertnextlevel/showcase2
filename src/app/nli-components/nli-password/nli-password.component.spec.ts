import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NliPasswordComponent } from './nli-password.component';

describe('NliPasswordComponent', () => {
  let component: NliPasswordComponent;
  let fixture: ComponentFixture<NliPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NliPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NliPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
