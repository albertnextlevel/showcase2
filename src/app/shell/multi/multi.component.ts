import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { components } from '@app/config/components.config';

@Component({
  selector: 'nli-multi',
  templateUrl: './multi.component.html',
  styleUrls: ['./multi.component.scss']
})
export class MultiComponent implements OnInit {

  @ViewChild('subnav') subnav!: MatSidenav;
  over = '';

  menu: any = components;

  constructor(private media: MediaObserver) { }

  ngOnInit() {
        const parent = this;
        this.media.media$
        .subscribe(function(this: any, change: MediaChange) {
          if(change.mqAlias === 'xs' || change.mqAlias === 'sm') {
            parent.subnav.close();
            parent.over = 'over';
          } else {
            parent.subnav.open();
            parent.over = 'side';
          }
        });
  }

  closeSubNav(): void {
    if(this.over === 'over') {this.subnav.close()}
  }

}
