import { HomeComponent } from './home/home.component';
import { FundamentalsComponent } from './fundamentals/fundamentals.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from './core';
import { Shell } from './shell/shell.service';
import { BestPracticesComponent } from './best-practices/best-practices.component';
import { EditorsComponent } from './editors/editors.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', component: HomeComponent, data: { title: extract('Home') } },
    { path: 'fundamentals', component: FundamentalsComponent, data: { title: extract('Fundamentals') } },
    { path: 'bestpractices', component: BestPracticesComponent, data: { title: extract('Best Practices') } },
    { path: 'editors', component: EditorsComponent, data: { title: extract('Editors') } },
  ]),
    // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
