import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nli-best-practices',
  templateUrl: './best-practices.component.html',
  styleUrls: ['./best-practices.component.scss']
})
export class BestPracticesComponent implements OnInit {

  importsSample = `import { LoaderService } from '../../../loader/loader.service';
import { environment } from '../../environment’;`;

  importsSample2 = `{ "compileOnSave": false, 
  "compilerOptions": { 
    "paths": { "@app/*": ["app/*"], 
               "@env/*": ["environments/*"] 
              } 
   } 
}`;
  importsSample3 = `import { LoaderService } from '@app/loader/loader.service'; 
import { environment } from '@env/environment’;`;


  constructor() { }

  ngOnInit() {
  }

}
