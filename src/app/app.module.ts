import { HomeComponent } from './home/home.component';
import { BestPracticesComponent } from './best-practices/best-practices.component';
import { FundamentalsComponent } from './fundamentals/fundamentals.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core';
import { MaterialModule } from './material.module';
import { NliComponentsModule } from './nli-components/nli-components.module';
import { SharedModule } from './shared';
import { ShellModule } from './shell/shell.module';


import { HighlightModule } from 'ngx-highlightjs';

import xml from 'highlight.js/lib/languages/xml';
import scss from 'highlight.js/lib/languages/scss';
import typescript from 'highlight.js/lib/languages/typescript';
import json from 'highlight.js/lib/languages/json';
import bash from 'highlight.js/lib/languages/bash';
import { EditorsComponent } from './editors/editors.component';

/**
 * Import every language you wish to highlight here
 * NOTE: The name of each language must match the file name its imported from
 */
export function hljsLanguages() {
  return [
    {name: 'typescript', func: typescript},
    {name: 'scss', func: scss},
    {name: 'xml', func: xml},
    {name: 'json', func: json},
    {name: 'bash', func: bash}
  ];
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FundamentalsComponent,
    BestPracticesComponent,
    EditorsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    ShellModule,
    NliComponentsModule,
    MaterialModule,
    HighlightModule.forRoot({
      languages: hljsLanguages
    }),
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
