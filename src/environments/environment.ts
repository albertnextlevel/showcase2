
export const environment = {
  production: false,
  defaultLanguage: 'en-US',
  supportedLanguages: [
    'en-US'
  ]
};