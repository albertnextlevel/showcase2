## Showcase APP

Run app with `npm start`

#### Adding new entries to NLI Components:
Link your npm to the yeoman generator-nli-demo-component by entering the folder and running `npm link`. ( Make sure you have yeoman installed globally first! )

After that you can simply run `yo nli-demo-component` inside the `src/app/nli-components` and it will generate a prewritten component for you.

Then all you need to do is add it inside the `/src/app/config/components.config.ts` app. ( Have a look at the others already declared :) 