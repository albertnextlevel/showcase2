"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const glob = require("glob");
const yosay = require("yosay");

module.exports = class extends Generator {
  async prompting() {
    let answers;

    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the lovely ${chalk.red(
          "generator-nli-demo-component"
        )} generator!
        Use this only from the main angular folder!`
      )
    );

    const prompts = [{
        type: "input",
        name: "name",
        message: "nli component name ? Start with uppercase without the nli prefix. Ex: Timeline"
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    const parent = this;
    glob(this.templatePath() + "/**/*", function (err, files) {
      files.forEach(file => {
        const originalFileName = file.replace(
          parent.templatePath().replace(/\\/gi, "/") + "/",
          ""
        );
        const newFileName = originalFileName.replace(
          /template/gi,
          String(parent.props.name).toLowerCase()
        );
        if (/\./gi.test(originalFileName)) {
          parent.fs.copyTpl(
            parent.templatePath(originalFileName),
            parent.destinationPath('nli-' + String(parent.props.name).toLowerCase() + '/nli-' + newFileName), {
              mainTitle: parent.props.name,
              secondaryTitle: String(parent.props.name).toLowerCase()
            }
          );
        }

      });
    });
  }

  install() {
    // this.installDependencies();
  }
};
