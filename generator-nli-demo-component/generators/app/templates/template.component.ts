import { Component, OnInit } from '@angular/core';

import { SetupModel } from '@app/shared/models/setup.model';
import { ApiModel } from '@app/shared/models/api.model';


@Component({
  selector: 'nli-nli-<%= secondaryTitle %>',
  templateUrl: './nli-<%= secondaryTitle %>.component.html',
  styleUrls: ['./nli-<%= secondaryTitle %>.component.scss']
})
export class Nli<%= mainTitle %>Component implements OnInit {


  usage: any = {
    ts: ``,
    html: ``,
    css: ``
  };

  setup: SetupModel[] = [
    {
      description: ``,
      code: ``,
      language: ''
    }
  ];

  api: ApiModel[] = [
    {
      type: '',
      items: [
        {
          name: '',
          description: ``,
          properties: [],
          methods: []
        }
      ]
    }
  ];


  constructor() {}

  ngOnInit() {}
}
