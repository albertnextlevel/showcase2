import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nli<%= mainTitle %>Component } from './nli-<%= secondaryTitle %>.component';

describe('Nli<%= mainTitle %>Component', () => {
  let component: Nli<%= mainTitle %>Component;
  let fixture: ComponentFixture<Nli<%= mainTitle %>Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nli<%= mainTitle %>Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nli<%= mainTitle %>Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
